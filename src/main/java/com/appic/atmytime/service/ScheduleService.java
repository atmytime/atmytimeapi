package com.appic.atmytime.service;

import com.appic.atmytime.model.Schedule;

import java.util.List;

public interface ScheduleService {
    Schedule save(Schedule schedule) throws Exception;
    List<Schedule> findAll() throws Exception;
    void delete(int id) throws Exception;
    Schedule findById(int id) throws Exception;
    Schedule update(Schedule schedule) throws Exception;
    void addBulkSchedules() throws Exception;
}
