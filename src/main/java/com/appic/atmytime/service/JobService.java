package com.appic.atmytime.service;

import com.appic.atmytime.model.Job;

import java.util.List;

public interface JobService {
    Job save(Job job) throws Exception;
    List<Job> findAll() throws Exception;
    void delete(int id) throws Exception;
    Job findById(int id) throws Exception;
    Job update(Job job) throws Exception;
    void addBulkJobs(int maxRows) throws Exception;
}
