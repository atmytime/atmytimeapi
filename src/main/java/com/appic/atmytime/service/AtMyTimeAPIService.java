package com.appic.atmytime.service;

import com.appic.atmytime.exception.BadRequestException;
import com.appic.atmytime.model.*;
import com.appic.atmytime.repository.UserRepository;
import com.appic.atmytime.util.AppConstants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AtMyTimeAPIService {

    @Autowired
    private UserRepository userRepository;

    protected final Log logger = LogFactory.getLog(this.getClass());

    private void validatePageNumberAndSize(int page, int size) {
        if(page < 0) {
            throw new BadRequestException("Page number cannot be less than zero.");
        }

        if(size > AppConstants.MAX_PAGE_SIZE) {
            throw new BadRequestException("Page size must not be greater than " + AppConstants.MAX_PAGE_SIZE);
        }
    }
}
