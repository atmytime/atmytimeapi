package com.appic.atmytime.service;

import com.appic.atmytime.model.Role;

import java.util.List;

public interface RoleService {
    Role save(Role role) throws Exception;
    List<Role> findAll() throws Exception;
    void delete(int id) throws Exception;
    Role findById(int id) throws Exception;
    Role update(Role role) throws Exception;
}
