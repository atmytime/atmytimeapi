package com.appic.atmytime.service;

import com.appic.atmytime.model.Location;

import java.util.List;

public interface LocationService {
    Location save(Location location) throws Exception;
    List<Location> findAll() throws Exception;
    void delete(int id) throws Exception;
    Location findById(int id) throws Exception;
    Location update(Location location) throws Exception;
    void addBulkLocations(int maxRows) throws Exception;
}
