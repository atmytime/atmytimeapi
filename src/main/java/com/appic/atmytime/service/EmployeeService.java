package com.appic.atmytime.service;

import com.appic.atmytime.model.Employee;

import java.util.List;

public interface EmployeeService {
    Employee save(Employee employee) throws Exception;
    List<Employee> findAll() throws Exception;
    void delete(int id) throws Exception;
    Employee findById(int id) throws Exception;
    Employee update(Employee employee) throws Exception;
    void addBulkEmployees(int maxRows);
}