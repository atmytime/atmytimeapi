package com.appic.atmytime.service;

import com.appic.atmytime.model.User;

import java.util.List;

public interface UserService {

    User save(User user) throws Exception;
    List<User> findAll() throws Exception;
    void delete(int id) throws Exception;
    User findOne(String userName) throws Exception;
    User findById(int id) throws Exception;
    User update(User userDto) throws Exception;
    void addBulkUsers(int maxRows) throws Exception;
}
