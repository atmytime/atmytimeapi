package com.appic.atmytime.service.impl;

import com.appic.atmytime.dao.JobDAO;
import com.appic.atmytime.model.Company;
import com.appic.atmytime.model.Job;
import com.appic.atmytime.service.JobService;
import com.appic.atmytime.util.RootCause;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Collections;
import java.util.Date;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Optional;
import java.util.ArrayList;

@Service(value = "jobService")
public class JobServiceImpl implements JobService {
    protected final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private JobDAO jobDAO;

    @Override
    public Job save(Job job) throws Exception {
        try {
            job.setCreatedDate(Calendar.getInstance().getTime());
            job.setCreatedUser("Admin");
            return jobDAO.save(job);
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public List<Job> findAll() throws Exception {
        try {
            List<Job> list = new ArrayList<>();
            jobDAO.findAll().iterator().forEachRemaining(list::add);
            return list;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            Job job = findById(id);
            job.setActive("N");
            update(job);
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public Job findById(int id) throws Exception {
        try {
            Optional<Job> optionalJob = jobDAO.findById(id);
            return optionalJob.isPresent() ? optionalJob.get() : null;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public Job update(Job job) throws Exception {
        try {
            logger.info("job: " + job);
            jobDAO.save(job);
            logger.info("After update job: " + job);
            return job;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public void addBulkJobs(int maxRows) throws Exception {
        for (int i=0; i<=maxRows; i++) {
            Job job = new Job();
            job.setActive("Y");
            job.setDescription("Job Description" + i);
            job.setEffectiveStartDate(new Date());
            job.setEffectiveEndDate(new Date());
            job.setName("Job Name - " + i);
            job.setCreatedDate(Calendar.getInstance().getTime());
            job.setCreatedUser("Admin");
            job.setUpdatedDate(new Date());
            job.setUpdatedUser("Admin");
            jobDAO.save(job);
        }
    }
}
