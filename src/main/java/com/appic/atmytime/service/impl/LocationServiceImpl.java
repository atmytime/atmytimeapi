package com.appic.atmytime.service.impl;

import com.appic.atmytime.dao.LocationDAO;
import com.appic.atmytime.model.Company;
import com.appic.atmytime.model.Location;
import com.appic.atmytime.service.LocationService;
import com.appic.atmytime.util.RootCause;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Collections;
import java.util.Date;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Optional;
import java.util.ArrayList;


@Service(value = "locationService")
public class LocationServiceImpl implements LocationService {
    protected final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private LocationDAO locationDAO;

    @Override
    public Location save(Location location) throws Exception {
        try {
            logger.info("location: " + location);
            location.setCreatedDate(Calendar.getInstance().getTime());
            location.setCreatedUser("Admin");
            return locationDAO.save(location);
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public List<Location> findAll() throws Exception {
        try {
        List<Location> list = new ArrayList<>();
        locationDAO.findAll().iterator().forEachRemaining(list::add);
        return list;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            Location location = findById(id);
            location.setActive("N");
            update(location);
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public Location findById(int id) throws Exception {
        try {
            Optional<Location> optionalLocation = locationDAO.findById(id);
            logger.info("optionalLocation for locationId: " + id + optionalLocation);
            return optionalLocation.isPresent() ? optionalLocation.get() : null;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public Location update(Location location) throws Exception {
        try {
            logger.info("location: " + location);
            locationDAO.save(location);
            logger.info("location after save: " + location);
            return location;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public void addBulkLocations(int maxRows) throws Exception {
        for (int i=0; i<=maxRows; i++) {
            Location location = new Location();
            location.setActive("Y");
            location.setDescription("Job Description" + i);
            location.setEffectiveStartDate(new Date());
            location.setEffectiveEndDate(new Date());
            location.setName("Location Name - " + i);
            location.setCity("City - " + i);
            location.setState("State - " + i);
            location.setStreetAddress("Street - " + i);
            location.setCountry("USA");
            location.setZipCode("12345");
            location.setCreatedDate(Calendar.getInstance().getTime());
            location.setCreatedUser("Admin");
            location.setUpdatedDate(new Date());
            location.setUpdatedUser("Admin");
            locationDAO.save(location);
        }
    }
}