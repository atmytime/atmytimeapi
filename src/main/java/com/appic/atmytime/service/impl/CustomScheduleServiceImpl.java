package com.appic.atmytime.service.impl;

import com.appic.atmytime.dao.CustomScheduleDAO;
import com.appic.atmytime.model.CustomSchedule;
import com.appic.atmytime.service.CustomScheduleService;
import com.appic.atmytime.util.RootCause;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service(value = "customScheduleService")
public class CustomScheduleServiceImpl implements CustomScheduleService {
    protected final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private CustomScheduleDAO customScheduleDAO;

    @Override
    public CustomSchedule save(CustomSchedule customSchedule) throws Exception {
        logger.info("customSchedule: " + customSchedule);
        try {
            customSchedule.setCreatedDate(Calendar.getInstance().getTime());
            customSchedule.setCreatedUser("Admin");
            customScheduleDAO.save(customSchedule);
        } catch (Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
        return customSchedule;
    }

    @Override
    public List<CustomSchedule> findAll() throws Exception {
        try {
            List<CustomSchedule> list = new ArrayList<>();
            customScheduleDAO.findAll().iterator().forEachRemaining(list::add);
            return list;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            CustomSchedule customSchedule = findById(id);
            //customSchedule.setActive("N");
            update(customSchedule);
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public CustomSchedule findById(int id) throws Exception {
        try {
            Optional<CustomSchedule> optionalSchedule = customScheduleDAO.findById(id);
            return optionalSchedule.isPresent() ? optionalSchedule.get() : null;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public CustomSchedule update(CustomSchedule customSchedule) throws Exception {
        try {
            logger.info("customSchedule: " + customSchedule);
            customScheduleDAO.save(customSchedule);
            return customSchedule;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public void addBulkCustomSchedules(int maxRows) throws Exception {
        for (int i=0; i<=maxRows; i++) {
            CustomSchedule customSchedule = new CustomSchedule();
            customSchedule.setCompanyId(i);
            customSchedule.setEmployeeId("COMPN" + i + "-" + i);
            customSchedule.setJobId(i);
            customSchedule.setLocationId(i);
            customSchedule.setScheduleStartDate(new Date());
            customSchedule.setScheduleEndDate(new Date());
            customSchedule.setScheduleStartTime("4:00 AM)");
            customSchedule.setScheduleEndTime("8:00 PM)");
            customSchedule.setScheduleId(i);
            customSchedule.setCreatedDate(Calendar.getInstance().getTime());
            customSchedule.setCreatedUser("Admin");
            customSchedule.setUpdatedDate(new Date());
            customSchedule.setUpdatedUser("Admin");
            customScheduleDAO.save(customSchedule);
        }
    }


}
