package com.appic.atmytime.service.impl;


import com.appic.atmytime.dao.LookupDAO;
import com.appic.atmytime.exception.AppException;
import com.appic.atmytime.model.Location;
import com.appic.atmytime.model.Lookup;
import com.appic.atmytime.service.LookupService;
import com.appic.atmytime.util.RootCause;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Collections;
import java.util.Date;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Optional;
import java.util.ArrayList;


@Service(value = "lookupService")
public class LookupServiceImpl implements LookupService {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private LookupDAO lookupDAO;

    public List<Lookup> findAll() throws Exception {
        try {
            List<Lookup> list = new ArrayList<>();
            lookupDAO.findAll().iterator().forEachRemaining(list::add);
            return list;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    public List<String> findByLookupType(String lookupType) throws Exception {
        try {
            List<String> list = new ArrayList<>();
            lookupDAO.findByLookupType(lookupType).iterator().forEachRemaining(list::add);
            logger.info("list: " + list);
            return list;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    public Lookup findById(int id) throws Exception {
        try {
            Optional<Lookup> optionalLocation = lookupDAO.findById(id);
            logger.info("optionalLocation for lookup Id: " + id + optionalLocation);
            return optionalLocation.isPresent() ? optionalLocation.get() : null;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public Lookup update(Lookup lookup) throws Exception {
        try {
            lookup = lookupDAO.save(lookup);
            return lookup;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public Lookup save(Lookup lookup) throws Exception {
        try {
            lookup.setActive("N");
            lookup.setCreatedUser("Admin");
            lookup.setCreatedDate(new Date());
            logger.info("newUser: " + lookup);
            return lookupDAO.save(lookup);
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            Lookup lookup = findById(id);
            lookup.setActive("N");
            update(lookup);
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

}