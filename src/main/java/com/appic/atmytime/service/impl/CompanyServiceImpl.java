package com.appic.atmytime.service.impl;

import com.appic.atmytime.dao.CompanyDAO;
import com.appic.atmytime.model.Company;
import com.appic.atmytime.service.CompanyService;
import com.appic.atmytime.util.RootCause;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Collections;
import java.util.Date;
import java.util.Arrays;
import java.util.Calendar;
import java.util.ArrayList;

@Service(value = "companyService")
public class CompanyServiceImpl implements CompanyService {
    protected final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private CompanyDAO companyDAO;

    @Override
    public Company save(Company company) throws Exception {
        try {
            company.setCreatedDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
            company.setCreatedUser("Admin");
            return companyDAO.save(company);
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public List<Company> findAll() throws Exception {
        try {
            List<Company> list = new ArrayList<>();
            companyDAO.findAll().iterator().forEachRemaining(list::add);
            return list;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            Company company = findById(id);
            company.setActive("N");
            update(company);
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public Company findById(int id) throws Exception {
        try {
            Company company = companyDAO.findById(id);
            return company;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public Company update(Company company) throws Exception {
        try {
            logger.info("company: " + company);
            companyDAO.save(company);
            return company;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public void addBulkCompanies(int maxRows) throws Exception {
        Calendar calendar = Calendar.getInstance();
        for (int i=0; i<=maxRows; i++) {
            Company company1 = new Company();
            company1.setName("Company Name" + "-" + i);
            company1.setDescription("Company Description" + "-" + i);
            company1.setCompanyCode("COMPN" + "-" + i);
            company1.setEffectiveStartDate(calendar.getTime());
            calendar.add(Calendar.DAY_OF_YEAR, 3600);
            company1.setEffectiveEndDate(calendar.getTime());
            company1.setStreetAddress("Company Street" + "-" + i);
            company1.setCity("Company City" + "-" + i);
            company1.setActive("Y");
            company1.setCountry("USA");
            company1.setCreatedDate(Calendar.getInstance().getTime());
            company1.setCreatedUser("Admin");
            company1.setFederalEIN(null);
            company1.setUpdatedDate(new Date());
            company1.setUpdatedUser("Admin");
            company1.setState("CO");
            company1.setZipCode("80134");
            //logger.info("company1: " + company1);
            companyDAO.save(company1);
        }
    }


}
