package com.appic.atmytime.service.impl;

import com.appic.atmytime.dao.RoleDAO;
import com.appic.atmytime.exception.AppException;
import com.appic.atmytime.model.Company;
import com.appic.atmytime.model.Role;
import com.appic.atmytime.model.RoleName;
import com.appic.atmytime.repository.RoleRepository;
import com.appic.atmytime.service.RoleService;
import com.appic.atmytime.util.RootCause;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;


@Service(value = "roleService")
public class RoleServiceImpl implements RoleService {
    protected final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private RoleDAO roleDAO;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role save(Role role) throws Exception {
        try {
            logger.info("role: " + role);
            role.setCreatedDate(Calendar.getInstance().getTime());
            role.setCreatedUser("Admin");
            return roleDAO.save(role);
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public List<Role> findAll() throws Exception {
        try {
            List<Role> list = new ArrayList<>();
            roleDAO.findAll().iterator().forEachRemaining(list::add);
            return list;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            Role role = findById(id);
            role.setActive("N");
            update(role);
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public Role findById(int id) throws Exception {
        try {
            Role role = roleRepository.findById(id).orElseThrow(() -> new AppException("User Role not set."));
            logger.info("role: " + role);
            return role;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public Role update(Role role) throws Exception {
        try {
            logger.info("role: " + role);
            Role roleFromDB = findById(role.getId());
            roleFromDB.setName(role.getName());
            roleFromDB.setDescription(role.getDescription());
            roleFromDB.setActive(role.getActive());
            roleFromDB.setUpdatedDate(role.getUpdatedDate());
            roleFromDB.setUpdatedUser(role.getUpdatedUser());
            roleDAO.save(roleFromDB);
            logger.info("roleFromDB after save: " + roleFromDB);
            return roleFromDB;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }
}