package com.appic.atmytime.service.impl;

import com.appic.atmytime.dao.CompanyDAO;
import com.appic.atmytime.dao.SequenceDAO;
import com.appic.atmytime.dao.UserDAO;
import com.appic.atmytime.dao.UserRoleDAO;
import com.appic.atmytime.model.Company;
import com.appic.atmytime.model.Sequence;
import com.appic.atmytime.model.UserRole;
import com.appic.atmytime.repository.UserRepository;
import com.appic.atmytime.model.User;
import com.appic.atmytime.repository.RoleRepository;
import com.appic.atmytime.service.UserService;
import com.appic.atmytime.util.RootCause;
import com.appic.atmytime.util.Util;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;


@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {
	protected final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private SequenceDAO sequenceDAO;

	@Autowired
	private CompanyDAO companyDAO;

	@Autowired
	private UserRoleDAO userRoleDAO;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		User user = userDAO.findByUserName(userName);
		if(user == null){
			throw new UsernameNotFoundException("Invalid userName or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), getAuthority());
	}

	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}

	public List<User> findAll() throws Exception {
		try {
			List<User> list = new ArrayList<>();
			Iterable<User> usersList = userDAO.findAll();
			usersList.iterator().forEachRemaining(list::add);
			logger.info("list: " + list);
			return list;
		} catch(Exception e) {
			logger.error("Exception occurred: ", e);
			throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
		}
	}

	@Override
	public void delete(int id) throws Exception {
		try {
			User user = findById(id);
			user.setActive("N");
			update(user);
		} catch (Exception e) {
			logger.error("Exception occurred: ", e);
			throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
		}
	}

	@Override
	public User findOne(String userName) throws Exception {
		try {
			logger.info("userName: " + userName);
			User user = userRepository.findByUserName(userName);
			logger.info("user: " + user);
			return user;
		} catch(Exception e) {
			logger.error("Exception occurred: ", e);
			throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
		}
	}

	@Override
	public User findById(int id) throws Exception {
		try {
			User user = userDAO.findById(id);
			return user;
		} catch(Exception e) {
			logger.error("Exception occurred: ", e);
			throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
		}
	}

    @Override
    public User update(User user) throws Exception {
		try {
			user.setUpdatedUser("Admin");
			user.setUpdatedDate(new Date());
			if (user.getPassword() == null) {
				user.setPassword(Util.generateRandomString());
			}
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			logger.info(passwordEncoder.encode(user.getPassword()) + ", " + user.getPassword());
			user = userDAO.save(user);
			logger.info("user: " + user);
			return user;
		} catch(Exception e) {
			logger.error("Exception occurred: ", e);
			throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
		}
    }

    @Override
    public User save(User user) throws Exception {
		try {
			logger.info("user.getPassword(): " + user.getPassword());
			if (user.getPassword() == null) {
				user.setPassword(Util.generateRandomString());
			}
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			logger.info(passwordEncoder.encode(user.getPassword()) + ", " + user.getPassword());

			Company company = companyDAO.findById(user.getCompanyId());
			user.setEmployeeId(generateEmployeeId(company.getId()));
			user.setCreatedUser("Admin");
			user.setCreatedDate(new Date());
			logger.info("user: " + user);
			user = userDAO.save(user);

			UserRole userRole = new UserRole();
			userRole.setRoleId(user.getRole().getId());
			userRole.setUserId(user.getId());
			userRole.setCreatedUser("Admin");
			userRole.setCreatedDate(new Date());

			return user;
		} catch(Exception e) {
			logger.error("Exception occurred: ", e);
			throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
		}
    }

	private String generateEmployeeId(int companyId) {
		Company company = companyDAO.findById(companyId);
		Long nextEmployeeSequence = getNextEmployeeSequence();
		logger.error("nextEmployeeSequence: " + nextEmployeeSequence);
		return company.getCompanyCode() + nextEmployeeSequence;
	}

	private Long getNextEmployeeSequence() {
		Sequence sequence = sequenceDAO.findAll().get(0);
		sequence.setSequenceCurrentValue(sequence.getSequenceCurrentValue()  + 1);
		sequenceDAO.save(sequence);
		return sequence.getSequenceCurrentValue();
	}

	@Override
	public void addBulkUsers(int maxRows) throws Exception {
		for (int i=0; i<=maxRows; i++) {
			User user = new User();
			user.setActive("Y");
			user.setCity("Denver");
			user.setState("CO");
			user.setCountry("USA");
			user.setCreatedDate(new Date());
			user.setCreatedUser("Admin");
			user.setEffectiveEndDate(new Date());
			user.setEffectiveStartDate(new Date());
			user.setEmployeeStatus("Regular");
			user.setEmployeeId(generateEmployeeId(user.getCompanyId()));
			user.setPassword("$2a$10$C2STNbzqfueOyrW7AGoSS.fatsABdpG3wMABu3OISyFXFV0j44AqW");
			user.setUserName("bulk_user" + i + "@test.com");
			user.setEmail("bulk_user" + i + "@test.com");
			user.setEffectiveEndDate(new Date());
			user.setEffectiveStartDate(new Date());
			user.setEmploymentType("Full Time");
			user.setFirstName("bulk_user" + i + "first name");
			user.setLastName("bulk_user" + i + "last name");
			Long number = (long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L;
			user.setPhone(number.toString());
			user.setZipCode("56543");
			user.setStreetAddress(i + "123 street");
			user = userDAO.save(user);

			UserRole userRole = new UserRole();
			userRole.setRoleId(1);
			userRole.setUserId(user.getId());
			userRole.setCreatedUser("Admin");
			userRole.setCreatedDate(new Date());
			userRoleDAO.save(userRole);


		}
	}
}