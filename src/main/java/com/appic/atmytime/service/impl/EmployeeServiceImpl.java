package com.appic.atmytime.service.impl;

import com.appic.atmytime.dao.EmployeeDAO;
import com.appic.atmytime.dao.UserDAO;
import com.appic.atmytime.model.Employee;
import com.appic.atmytime.service.EmployeeService;
import com.appic.atmytime.util.RootCause;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service(value = "employeeService")
public class EmployeeServiceImpl implements EmployeeService {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private EmployeeDAO employeeDAO;

    @Autowired
    private UserDAO userDAO;

    @Override
    public Employee save(Employee employee) throws Exception {
        try {
            employee.setCreatedDate(Calendar.getInstance().getTime());
            employee.setCreatedUser("Admin");
            Employee createdEmployee = employeeDAO.save(employee);
            return createdEmployee;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public List<Employee> findAll() throws Exception {
        try {
            List<Employee> list = new ArrayList<>();
            employeeDAO.findAll().iterator().forEachRemaining(list::add);
            return list;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            Employee employee = findById(id);
            employee.setActive("N");
            update(employee);
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public Employee findById(int id) throws Exception {
        try {
            Optional<Employee> optionalEmployee = employeeDAO.findById(id);
            return optionalEmployee.isPresent() ? optionalEmployee.get() : null;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public Employee update(Employee employee) throws Exception {
        try {
            employeeDAO.saveAndFlush(employee);
            return employee;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public void addBulkEmployees(int maxRows) {

    }
}
