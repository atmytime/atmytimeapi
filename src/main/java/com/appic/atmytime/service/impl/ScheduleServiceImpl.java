package com.appic.atmytime.service.impl;

import com.appic.atmytime.dao.ScheduleDAO;
import com.appic.atmytime.model.Company;
import com.appic.atmytime.model.Schedule;
import com.appic.atmytime.service.ScheduleService;
import com.appic.atmytime.util.RootCause;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Collections;
import java.util.Date;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Optional;
import java.util.ArrayList;


@Service(value = "scheduleService")
public class ScheduleServiceImpl implements ScheduleService {
    protected final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private ScheduleDAO scheduleDAO;

    @Override
    public Schedule save(Schedule schedule) throws Exception {
        try {
            schedule.setCreatedDate(Calendar.getInstance().getTime());
            schedule.setCreatedUser("Admin");
            scheduleDAO.save(schedule);
        } catch (Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
        return schedule;
    }

    @Override
    public List<Schedule> findAll() throws Exception {
        try {
            List<Schedule> list = new ArrayList<>();
            scheduleDAO.findAll().iterator().forEachRemaining(list::add);
            return list;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public void delete(int id) throws Exception {
        try {
            Schedule schedule = findById(id);
            schedule.setActive("N");
            update(schedule);
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public Schedule findById(int id) throws Exception {
        try {
            Optional<Schedule> optionalSchedule = scheduleDAO.findById(id);
            return optionalSchedule.isPresent() ? optionalSchedule.get() : null;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public Schedule update(Schedule schedule) throws Exception {
        try {
            logger.info("schedule: " + schedule);
            scheduleDAO.save(schedule);
            return schedule;
        } catch(Exception e) {
            logger.error("Exception occurred: ", e);
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @Override
    public void addBulkSchedules() throws Exception {

    }
}
