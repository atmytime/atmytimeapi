package com.appic.atmytime.service;


import com.appic.atmytime.model.Lookup;

import java.util.List;

public interface LookupService {
    Lookup save(Lookup lookup) throws Exception;
    List<Lookup> findAll() throws Exception;
    List<String> findByLookupType(String lookupType) throws Exception;
    Lookup findById(int id) throws Exception;
    Lookup update(Lookup lookup) throws Exception;
    void delete(int id) throws Exception;
}
