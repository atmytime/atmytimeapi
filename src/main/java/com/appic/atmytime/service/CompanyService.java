package com.appic.atmytime.service;

import com.appic.atmytime.model.Company;

import java.util.List;

public interface CompanyService {
    Company save(Company company) throws Exception;
    List<Company> findAll() throws Exception;
    void delete(int id) throws Exception;
    Company findById(int id) throws Exception;
    Company update(Company company) throws Exception;
    void addBulkCompanies(int maxRows) throws Exception;
}