package com.appic.atmytime.service;

import com.appic.atmytime.model.CustomSchedule;

import java.util.List;

public interface CustomScheduleService {
    CustomSchedule save(CustomSchedule customSchedule) throws Exception;
    List<CustomSchedule> findAll() throws Exception;
    void delete(int id) throws Exception;
    CustomSchedule findById(int id) throws Exception;
    CustomSchedule update(CustomSchedule customSchedule) throws Exception;
    void addBulkCustomSchedules(int maxRows) throws Exception;
}
