package com.appic.atmytime.controller;

import com.appic.atmytime.model.ApiResponse;
import com.appic.atmytime.model.Schedule;
import com.appic.atmytime.service.ScheduleService;
import com.appic.atmytime.util.RootCause;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/schedules")
public class ScheduleController {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private ScheduleService scheduleService;

    @PostMapping
    public ApiResponse<Schedule> saveSchedule(@RequestBody Schedule schedule) {
        try {
            logger.info("schedule: " + schedule);
            return new ApiResponse<>(HttpStatus.OK.value(), "Schedule saved successfully.",scheduleService.save(schedule));
        } catch (Exception e) {
            logger.error("Exception occurred: ", e);
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", e.getLocalizedMessage());
        }
    }

    @GetMapping
    public ApiResponse<List<Schedule>> findAll() throws Exception {
        try {
            return new ApiResponse<>(HttpStatus.OK.value(), "Schedule list fetched successfully.",scheduleService.findAll());
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @GetMapping("/{id}")
    public ApiResponse<Schedule> findById(@PathVariable int id) throws Exception {
        try {
            logger.info("id: " + id);
            return new ApiResponse<>(HttpStatus.OK.value(), "Schedule fetched successfully.",scheduleService.findById(id));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @PutMapping
    public ApiResponse<Schedule> update(@RequestBody Schedule schedule) throws Exception {
        try {
            logger.info("schedule: " + schedule);
            return new ApiResponse<>(HttpStatus.OK.value(), "Schedule updated successfully.",scheduleService.update(schedule));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) throws Exception {
        try {
            logger.info("id: " + id);
            scheduleService.delete(id);
            return new ApiResponse<>(HttpStatus.OK.value(), "Schedule active successfully.", null);
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @PostMapping("/bulk")
    public ApiResponse<Schedule> addBulkSchedules() throws Exception {
        try {
            scheduleService.addBulkSchedules();
            return new ApiResponse<Schedule>(HttpStatus.OK.value(), "Schedules saved successfully.", "Schedules saved successfully");
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }
}
