package com.appic.atmytime.controller;

import com.appic.atmytime.config.JwtTokenUtil;
import com.appic.atmytime.model.*;
import com.appic.atmytime.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.context.SecurityContextHolder;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/signIn", method = RequestMethod.POST)
    public ApiResponse<AuthToken> signIn(@RequestBody LoginUser loginUser) throws Exception {
        logger.info("loginUser: " + loginUser);
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUser.getUserName(), loginUser.getPassword()));
        final User user = userService.findOne(loginUser.getUserName());
        final String token = jwtTokenUtil.generateToken(user);
        return new ApiResponse<>(200, "success", new AuthToken(token, user));
    }

    @RequestMapping(value = "/signUp", method = RequestMethod.POST)
    public ApiResponse<User> signUp(@RequestBody User user) throws Exception {
        logger.info("user: " + user);
        userService.save(user);
        return new ApiResponse<>(200, "success", user);
    }

    @RequestMapping(value = "/userFromToken", method = RequestMethod.POST)
    public ApiResponse<User> getUerFromToken() throws Exception {
        logger.info("getUerFromToken()...");
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("userName: " + userName);
        return new ApiResponse<>(200, "success", userService.findOne(userName));
    }
}