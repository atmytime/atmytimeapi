package com.appic.atmytime.controller;

import com.appic.atmytime.model.ApiResponse;
import com.appic.atmytime.model.Job;
import com.appic.atmytime.service.JobService;
import com.appic.atmytime.util.RootCause;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/jobs")
public class JobController {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private JobService jobService;

    @PostMapping
    public ApiResponse<Job> saveJob(@RequestBody Job job) throws Exception {
        try {
            logger.info("job: " + job);
            return new ApiResponse<>(HttpStatus.OK.value(), "Job saved successfully.",jobService.save(job));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @PutMapping
    public ApiResponse<Job> update(@RequestBody Job job) throws Exception {
        try {
            logger.info("job: " + job);
            return new ApiResponse<>(HttpStatus.OK.value(), "Job updated successfully.",jobService.update(job));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @PatchMapping("/{id}")
    public ApiResponse<Job> updateJob(@RequestBody Job job, @PathVariable("id") String id) throws Exception {
        try {
            logger.info("id: " + id);
            return new ApiResponse<>(HttpStatus.OK.value(), "Job saved successfully.",jobService.update(job));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @GetMapping
    public ApiResponse<List<Job>> findAll() throws Exception {
        try {
            return new ApiResponse<>(HttpStatus.OK.value(), "Job list fetched successfully.",jobService.findAll());
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @GetMapping("/{id}")
    public ApiResponse<Job> findById(@PathVariable int id) throws Exception {
        try {
            logger.info("id: " + id);
            return new ApiResponse<>(HttpStatus.OK.value(), "Job fetched successfully.",jobService.findById(id));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) throws Exception {
        try {
            logger.info("id: " + id);
            jobService.delete(id);
            return new ApiResponse<>(HttpStatus.OK.value(), "Job active successfully.", null);
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @PostMapping("/bulk/{maxRows}")
    public ApiResponse<Job> addBulkJobs(@PathVariable int maxRows) throws Exception {
        try {
            jobService.addBulkJobs(maxRows);
            return new ApiResponse<Job>(HttpStatus.OK.value(), "Jobs saved successfully.", "Jobs saved successfully");
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }
}
