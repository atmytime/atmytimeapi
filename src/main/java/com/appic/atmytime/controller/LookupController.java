package com.appic.atmytime.controller;

import com.appic.atmytime.config.JwtTokenUtil;
import com.appic.atmytime.model.ApiResponse;
import com.appic.atmytime.model.Lookup;
import com.appic.atmytime.service.LookupService;
import com.appic.atmytime.util.RootCause;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/lookups")
public class LookupController {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private LookupService lookupService;

    @PostMapping
    public ApiResponse<Lookup> saveLookup(@RequestBody Lookup lookup) throws Exception {
        try {
            return new ApiResponse<>(HttpStatus.OK.value(), "Lookup saved successfully.",lookupService.save(lookup));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @GetMapping
    public ApiResponse<List<Lookup>> listLookup() throws Exception {
        try {
            return new ApiResponse<>(HttpStatus.OK.value(), "Lookup list fetched successfully.",lookupService.findAll());
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @GetMapping("/lookupType/{lookupType}")
    public ApiResponse<List<String>> listLookupByLookupType(@PathVariable String lookupType) throws Exception {
        try {
            return new ApiResponse<>(HttpStatus.OK.value(), "Lookup list fetched successfully.",lookupService.findByLookupType(lookupType));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @GetMapping("/{id}")
    public ApiResponse<Lookup> getOne(@PathVariable int id) throws Exception {
        try {
            return new ApiResponse<>(HttpStatus.OK.value(), "Lookup fetched successfully.",lookupService.findById(id));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @PutMapping
    public ApiResponse<Lookup> update(@RequestBody Lookup lookup) throws Exception {
        try {
            logger.info("lookup: " + lookup);
            return new ApiResponse<>(HttpStatus.OK.value(), "Lookup updated successfully.",lookupService.update(lookup));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) throws Exception {
        try {
            logger.info("id: " + id);
            lookupService.delete(id);
            return new ApiResponse<>(HttpStatus.OK.value(), "Location active successfully.", null);
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }



}
