package com.appic.atmytime.controller;

import com.appic.atmytime.model.ApiResponse;
import com.appic.atmytime.model.Location;
import com.appic.atmytime.model.Role;
import com.appic.atmytime.service.LocationService;
import com.appic.atmytime.service.RoleService;
import com.appic.atmytime.util.RootCause;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/roles")
public class RoleController {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private RoleService roleService;

    @PostMapping
    public ApiResponse<Role> saveRole(@RequestBody Role role) throws Exception {
        try {
            logger.info("role: " + role);
            return new ApiResponse<>(HttpStatus.OK.value(), "Role saved successfully.", roleService.save(role));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @GetMapping
    public ApiResponse<List<Role>> findAll() throws Exception {
        try {
            return new ApiResponse<>(HttpStatus.OK.value(), "Role list fetched successfully.",roleService.findAll());
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @GetMapping("/{id}")
    public ApiResponse<Role> findById(@PathVariable int id) throws Exception {
        try {
            logger.info("id: " + id);
            return new ApiResponse<>(HttpStatus.OK.value(), "Role fetched successfully.",roleService.findById(id));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @PutMapping
    public ApiResponse<Role> update(@RequestBody Role roleDto) throws Exception {
        try {
            logger.info("roleDto: " + roleDto);
            return new ApiResponse<>(HttpStatus.OK.value(), "Role updated successfully.", roleService.update(roleDto));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) throws Exception {
        try {
            logger.info("id: " + id);
            roleService.delete(id);
            return new ApiResponse<>(HttpStatus.OK.value(), "Role active successfully.", null);
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }
}