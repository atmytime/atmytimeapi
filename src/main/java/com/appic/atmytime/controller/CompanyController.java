package com.appic.atmytime.controller;

import com.appic.atmytime.model.ApiResponse;
import com.appic.atmytime.model.Company;
import com.appic.atmytime.service.CompanyService;
import com.appic.atmytime.util.RootCause;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/companies")
public class CompanyController {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private CompanyService companyService;

    @PostMapping
    public ApiResponse<Company> saveCompany(@RequestBody Company company) throws Exception {
        try {
            logger.info("company: " + company);
            return new ApiResponse<Company>(HttpStatus.OK.value(), "Company saved successfully.",companyService.save(company));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @GetMapping
    public ApiResponse<List<Company>> findAll() throws Exception {
        try {
            return new ApiResponse<List<Company>>(HttpStatus.OK.value(), "Company list fetched successfully.",companyService.findAll());
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @GetMapping("/{id}")
    public ApiResponse<Company> findById(@PathVariable int id) throws Exception {
        try {
            logger.info("id: " + id);
            return new ApiResponse<Company>(HttpStatus.OK.value(), "Company fetched successfully.",companyService.findById(id));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @PutMapping
    public ApiResponse<Company> update(@RequestBody Company companyDto) throws Exception {
        try {
            logger.info("companyDto: " + companyDto);
            return new ApiResponse<Company>(HttpStatus.OK.value(), "Company updated successfully.",companyService.update(companyDto));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            throw new RuntimeException(RootCause.findCauseUsingPlainJava(e));
        }
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) throws Exception {
        try {
            logger.info("id: " + id);
            companyService.delete(id);
            return new ApiResponse<Void>(HttpStatus.OK.value(), "Company active successfully.", null);
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @PostMapping("/bulk/{maxRows}")
    public ApiResponse<Company> addBulkCompanies(@PathVariable int maxRows) throws Exception {
        try {
            companyService.addBulkCompanies(maxRows);
            return new ApiResponse<Company>(HttpStatus.OK.value(), "Companies saved successfully.", "Companies saved successfully");
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

}
