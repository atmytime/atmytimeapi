package com.appic.atmytime.controller;

import com.appic.atmytime.model.ApiResponse;
import com.appic.atmytime.model.Employee;
import com.appic.atmytime.service.EmployeeService;
import com.appic.atmytime.util.RootCause;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/employees")
public class EmployeeController {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private EmployeeService employeeService;

    @PostMapping
    public ApiResponse<Employee> saveEmployee(@RequestBody Employee employee) throws Exception {
        try {
            logger.info("employee: " + employee);
            return new ApiResponse<>(HttpStatus.OK.value(), "Employee saved successfully.",employeeService.save(employee));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @GetMapping
    public ApiResponse<List<Employee>> findAll() throws Exception {
        try {
            return new ApiResponse<>(HttpStatus.OK.value(), "Employee list fetched successfully.",employeeService.findAll());
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @GetMapping("/{id}")
    public ApiResponse<Employee> findById(@PathVariable int id) throws Exception {
        try {
            logger.info("id: " + id);
            return new ApiResponse<>(HttpStatus.OK.value(), "Employee fetched successfully.",employeeService.findById(id));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @PutMapping
    public ApiResponse<Employee> update(@RequestBody Employee employee) throws Exception {
        try {
            logger.info("employee: " + employee);
            return new ApiResponse<>(HttpStatus.OK.value(), "Employee updated successfully.",employeeService.update(employee));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) throws Exception {
        try {
            logger.info("id: " + id);
            employeeService.delete(id);
            return new ApiResponse<>(HttpStatus.OK.value(), "Employee active successfully.", null);
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @PostMapping("/bulk/{maxRows}")
    public ApiResponse<Employee> addBulkCompanies(@PathVariable int maxRows) throws Exception {
        try {
            employeeService.addBulkEmployees(maxRows);
            return new ApiResponse<Employee>(HttpStatus.OK.value(), "Employees saved successfully.", "Employees saved successfully");
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }
}
