package com.appic.atmytime.controller;

import com.appic.atmytime.model.ApiResponse;
import com.appic.atmytime.model.CustomSchedule;
import com.appic.atmytime.service.CustomScheduleService;
import com.appic.atmytime.util.RootCause;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/customSchedules")
public class CustomScheduleController {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private CustomScheduleService customScheduleService;

    /*
    @PostMapping
    public ApiResponse<CustomSchedule> saveSchedule(@RequestBody CustomSchedule customSchedule) {
        try {
            logger.info("customSchedule: " + customSchedule);
            return new ApiResponse<>(HttpStatus.OK.value(), "CustomSchedule saved successfully.", customScheduleService.save(customSchedule));
        } catch (Exception e) {
            logger.error("Exception occurred: ", e);
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", e.getLocalizedMessage());
        }
    }
    */

    @PostMapping
    public ApiResponse<CustomSchedule> saveMultipleSchedules(@RequestBody List<CustomSchedule> customSchedules) {
        try {
            logger.info("customSchedules: " + customSchedules);
            for (CustomSchedule customSchedule: customSchedules) {
                customScheduleService.save(customSchedule);
            }
            return new ApiResponse<>(HttpStatus.OK.value(), "CustomSchedule saved successfully.", null);
        } catch (Exception e) {
            logger.error("Exception occurred: ", e);
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", e.getLocalizedMessage());
        }
    }

    @GetMapping
    public ApiResponse<List<CustomSchedule>> findAll() throws Exception {
        try {
            return new ApiResponse<>(HttpStatus.OK.value(), "CustomSchedule list fetched successfully.",customScheduleService.findAll());
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @GetMapping("/{id}")
    public ApiResponse<CustomSchedule> findById(@PathVariable int id) throws Exception {
        try {
            logger.info("id: " + id);
            return new ApiResponse<>(HttpStatus.OK.value(), "CustomSchedule fetched successfully.",customScheduleService.findById(id));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @PutMapping
    public ApiResponse<CustomSchedule> update(@RequestBody CustomSchedule customSchedule) throws Exception {
        try {
            logger.info("customSchedule: " + customSchedule);
            return new ApiResponse<>(HttpStatus.OK.value(), "CustomSchedule updated successfully.",customScheduleService.update(customSchedule));
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) throws Exception {
        try {
            logger.info("id: " + id);
            customScheduleService.delete(id);
            return new ApiResponse<>(HttpStatus.OK.value(), "CustomSchedule active successfully.", null);
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @PostMapping("/bulk/{maxRows}")
    public ApiResponse<CustomSchedule> addBulkCompanies(@PathVariable int maxRows) throws Exception {
        try {
            customScheduleService.addBulkCustomSchedules(maxRows);
            return new ApiResponse<CustomSchedule>(HttpStatus.OK.value(), "CustomSchedules saved successfully.", "CustomSchedules saved successfully");
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }
}