package com.appic.atmytime.controller;

import com.appic.atmytime.config.JwtTokenUtil;
import com.appic.atmytime.model.*;
import com.appic.atmytime.service.UserService;
import com.appic.atmytime.util.RootCause;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/users")
public class UserController {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private UserService userService;

    @PostMapping
    public ApiResponse<User> saveUser(@RequestBody User user) throws Exception {
        logger.info("user: " + user);
        try {
            return new ApiResponse<>(HttpStatus.OK.value(), "User saved successfully.", userService.save(user));
        } catch (Exception e) {
            logger.error("Exception occurred: ", e);
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @GetMapping
    public ApiResponse<List<User>> listUser() throws Exception {
        try {
            return new ApiResponse<>(HttpStatus.OK.value(), "User list fetched successfully.",userService.findAll());
        } catch (Exception e) {
            logger.error("Exception occurred: ", e);
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @GetMapping("/{id}")
    public ApiResponse<User> getOne(@PathVariable int id) throws Exception {
        try {
            return new ApiResponse<>(HttpStatus.OK.value(), "User fetched successfully.",userService.findById(id));
        } catch (Exception e) {
            logger.error("Exception occurred: ", e);
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @PutMapping
    public ApiResponse<User> update(@RequestBody User user) throws Exception {
        try {
            logger.info("user: " + user);
            return new ApiResponse<>(HttpStatus.OK.value(), "User updated successfully.",userService.update(user));
        } catch (Exception e) {
            logger.error("Exception occurred: ", e);
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) throws Exception {
        try {
            userService.delete(id);
            return new ApiResponse<>(HttpStatus.OK.value(), "User active successfully.", null);
        } catch (Exception e) {
            logger.error("Exception occurred: ", e);
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }

    @PostMapping("/bulk/{maxRows}")
    public ApiResponse<User> addBulkUsers(@PathVariable int maxRows) throws Exception {
        try {
            userService.addBulkUsers(maxRows);
            return new ApiResponse<User>(HttpStatus.OK.value(), "Users saved successfully.", "Users saved successfully");
        } catch(Exception e) {
            logger.error("Exception occurred: " + RootCause.findCauseUsingPlainJava(e));
            return new ApiResponse<>(HttpStatus.UNPROCESSABLE_ENTITY.value(), "Error Occurred: ", RootCause.findCauseUsingPlainJava(e));
        }
    }
}