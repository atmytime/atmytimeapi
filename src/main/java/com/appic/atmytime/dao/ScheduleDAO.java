package com.appic.atmytime.dao;

import com.appic.atmytime.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ScheduleDAO extends JpaRepository<Schedule, Integer> {

    Optional<Schedule> findById(int id);
}
