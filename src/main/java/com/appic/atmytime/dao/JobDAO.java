package com.appic.atmytime.dao;


import com.appic.atmytime.model.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JobDAO extends JpaRepository<Job, Integer> {

    Optional<Job> findById(int id);
}