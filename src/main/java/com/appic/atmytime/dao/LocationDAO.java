package com.appic.atmytime.dao;

import com.appic.atmytime.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LocationDAO extends JpaRepository<Location, Integer> {

    Optional<Location> findById(int id);
}