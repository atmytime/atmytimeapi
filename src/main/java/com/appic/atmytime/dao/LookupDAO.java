package com.appic.atmytime.dao;

import com.appic.atmytime.model.Location;
import com.appic.atmytime.model.Lookup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LookupDAO extends JpaRepository<Lookup, Integer> {

    Optional<Lookup> findById(int id);

    @Query("SELECT l.lookupValue FROM Lookup l where l.lookupType = ?1")
    List<String> findByLookupType(String lookupType);
}