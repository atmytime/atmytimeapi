package com.appic.atmytime.dao;

import com.appic.atmytime.model.Lookup;
import com.appic.atmytime.model.Sequence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SequenceDAO extends JpaRepository<Sequence, Integer> {

    @Query("SELECT s.sequenceCurrentValue + 1 FROM Sequence s where s.sequenceName = ?1")
    Long getNextVal(String sequenceName);
}