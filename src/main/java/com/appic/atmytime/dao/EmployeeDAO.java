package com.appic.atmytime.dao;

import com.appic.atmytime.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeDAO extends JpaRepository<Employee, Integer> {

    Optional<Employee> findById(int id);
}