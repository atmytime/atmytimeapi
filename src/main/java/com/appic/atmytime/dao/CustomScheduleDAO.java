package com.appic.atmytime.dao;

import com.appic.atmytime.model.CustomSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomScheduleDAO extends JpaRepository<CustomSchedule, Integer> {

    Optional<CustomSchedule> findById(int id);
}
