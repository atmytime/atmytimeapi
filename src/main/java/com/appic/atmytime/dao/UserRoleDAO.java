package com.appic.atmytime.dao;

import com.appic.atmytime.model.User;
import com.appic.atmytime.model.UserRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRoleDAO extends CrudRepository<UserRole, Long> {

    Optional<UserRole> findById(Long id);
}
