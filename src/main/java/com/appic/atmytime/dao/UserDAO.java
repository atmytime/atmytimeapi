package com.appic.atmytime.dao;

import com.appic.atmytime.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDAO extends CrudRepository<User, Long> {

    User findByUserName(String userName);

    User findById(long id);
}
