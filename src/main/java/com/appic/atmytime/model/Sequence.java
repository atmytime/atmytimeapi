package com.appic.atmytime.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "sequence_data")
public class Sequence {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "sequence_name")
    private String sequenceName;

    @Column(name = "sequence_current_value")
    private Long sequenceCurrentValue;

    public Sequence() {
    }

    public Sequence(String employeeSequence, Long sequence) {
        this.sequenceName = employeeSequence;
        this.sequenceCurrentValue = sequence;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSequenceName() {
        return sequenceName;
    }

    public void setSequenceName(String sequenceName) {
        this.sequenceName = sequenceName;
    }

    public Long getSequenceCurrentValue() {
        return sequenceCurrentValue;
    }

    public void setSequenceCurrentValue(Long sequenceCurrentValue) {
        this.sequenceCurrentValue = sequenceCurrentValue;
    }

    @Override
    public String toString() {
        return "Sequence{" +
                "id=" + id +
                ", sequenceName='" + sequenceName + '\'' +
                ", sequenceCurrentValue=" + sequenceCurrentValue +
                '}';
    }
}