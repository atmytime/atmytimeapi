package com.appic.atmytime.model;

import java.util.HashSet;
import java.util.Set;

public class AuthToken {

    private String accessToken;
    private User user;

    public AuthToken(String accessToken, User user) {
        this.accessToken = accessToken;
        this.user = user;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
