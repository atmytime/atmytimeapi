package com.appic.atmytime.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "custom_schedule")
public class CustomSchedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "EMPLOYEE_ID")
    private String employeeId;

    @Column(name = "COMPANY_ID")
    private int companyId;

    @Column(name = "LOCATION_ID")
    private int locationId;

    @Column(name = "JOB_ID")
    private int jobId;

    @Column(name = "SCHEDULE_ID")
    private int scheduleId;

    @Column(name = "SCHEDULE_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduleEndDate;

    @Column(name = "SCHEDULE_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduleStartDate;

    @Column(name = "SCHEDULE_START_TIME")
    private String scheduleStartTime;

    @Column(name = "SCHEDULE_END_TIME")
    private String scheduleEndTime;

    @Column(name = "UPDATED_USER")
    private String updatedUser;

    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;

    @Column(name = "CREATED_USER")
    private String createdUser;

    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public int getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Date getScheduleEndDate() {
        return scheduleEndDate;
    }

    public void setScheduleEndDate(Date scheduleEndDate) {
        this.scheduleEndDate = scheduleEndDate;
    }

    public Date getScheduleStartDate() {
        return scheduleStartDate;
    }

    public void setScheduleStartDate(Date scheduleStartDate) {
        this.scheduleStartDate = scheduleStartDate;
    }

    public String getScheduleStartTime() {
        return scheduleStartTime;
    }

    public void setScheduleStartTime(String scheduleStartTime) {
        this.scheduleStartTime = scheduleStartTime;
    }

    public String getScheduleEndTime() {
        return scheduleEndTime;
    }

    public void setScheduleEndTime(String scheduleEndTime) {
        this.scheduleEndTime = scheduleEndTime;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "CustomSchedule{" +
                "id=" + id +
                ", employeeId='" + employeeId + '\'' +
                ", companyId=" + companyId +
                ", locationId=" + locationId +
                ", jobId=" + jobId +
                ", scheduleId=" + scheduleId +
                ", scheduleEndDate=" + scheduleEndDate +
                ", scheduleStartDate=" + scheduleStartDate +
                ", scheduleStartTime='" + scheduleStartTime + '\'' +
                ", scheduleEndTime='" + scheduleEndTime + '\'' +
                ", updatedUser='" + updatedUser + '\'' +
                ", updatedDate=" + updatedDate +
                ", createdUser='" + createdUser + '\'' +
                ", createdDate=" + createdDate +
                '}';
    }
}
