package com.appic.atmytime.model;

public enum  RoleName {
    Employee,
    Manager,
    Admin,
    SuperAdmin,
    Owner
}
