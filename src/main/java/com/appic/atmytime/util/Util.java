package com.appic.atmytime.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.lang3.RandomStringUtils;

import java.nio.charset.Charset;
import java.util.Random;

public class Util {
    protected static Log logger = LogFactory.getLog(Util.class);
    public static String generateRandomString() {
        byte[] array = new byte[7]; // length is bounded by 7
        String generatedString = RandomStringUtils.randomAlphanumeric(10);
        logger.info("generatedString: " + generatedString);
        return generatedString;
    }
}
