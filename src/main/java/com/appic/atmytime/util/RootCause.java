package com.appic.atmytime.util;

import java.util.Objects;

public class RootCause {
    public static String findCauseUsingPlainJava(Throwable throwable) throws Exception {
        Objects.requireNonNull(throwable);
        Throwable rootCause = throwable;
        while (rootCause.getCause() != null && rootCause.getCause() != rootCause) {
            rootCause = rootCause.getCause();
        }
        return rootCause.getLocalizedMessage();
    }
}
